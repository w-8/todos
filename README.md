# todos

#### 介绍
rust egui ,模仿iced的tods示例做的 egui版的todos 

#### 软件架构
rust + egui 即时刷新模式

#### 使用说明

1.  安装 rust环境
2.  cargo install todos_egui
3.  ./todos_egui

#### 展示

![todos程序](asset/todos.gif)

